# Foire aux questions


## Organisation

### Qui doit participer ?

L'utilisation du protocole ChatonsInfos repose sur le volontariat.
Aucun membre du collectif CHATONS n'a l'obligation de l'adopter.

### ChatonsInfos va-t-il remplacer la revue annuelle des fiches adhérents ?

Non. Même si certaines informations sont redondantes, ChatonsInfos se veut complémentaire.

La revue annuelle est obligatoire. ChatonsInfos repose sur le volontariat.


## Le site stats.chatons.org

### Quel est le but de stats.chatons.org

Le but de stats.chatons.org est de valoriser les informations partagées volontairement par les membres du collectif CHATONS.

Nous pensons que ces données permettront de mieux connaître l'activité du collectif, ses membres et leurs services. Et donc de mieux le faire connaître et d'orienter ses stratégies.

Par exemple, détecter de la demande sur un service qui n'est que peu proposé, ou au contraire, constater le faible usage d'un service qui n'aurait pas besoin de plus d'instances.

### Cela ne va-t-il pas créer de la compétition entre membre du collectif CHATONS ?

Non. De l'émulation, peut-être. De la coordination et de la mutualisation, certainement. Les valeurs du collectif CHATONS sont l'entraide, la transparence, la solidarité, etc. Donc stats.chatons.org est un d'abord un outil de cohésion.

### Je ne partage pas encore de ficher _properties_, suis-je sur stats.chatons.org ?

Oui. Nous créons un fichier par défaut par membre du collectif CHATONS. Ce fichier est rempli _a minima_ avec notamment un logo de chat tout mignon généré via le [CatAvatarGenerator](https://www.peppercarrot.com/extras/html/2016_cat-generator/index.php) de David Revoy .

Encouragements appuyés à créer votre propre fichier qui contiendra des données bien plus intéressantes.

### Est-ce que des personnes pourront s'en servir pour trouver un service ?

Oui. À partir du moment où des données sur les services des membres sont disponibles, les gens sont libres de les utiliser pour découvrir et choisir un service. Mais stats.chatons.org ne remplacera par les formulaires de recherches de [chatons.org](https://www.chatons.org/).


## Fichiers _properties_

Pour débuter avec le ChatonsInfos et les fichiers _properties_, les informations se trouvent dans le fichier [README.md](./README.md) section « BIEN COMMENCER ».

### Quelles contraintes pour les images ?

ChatonsInfos s'appuie sur des fichiers _properties_ pouvant contenir des liens vers des images (logo d'une organization, d'un service…).

Les contraintes sur ces fichiers images sont :
* format : affichable par un navigateur (svg, png, jpg, gif), recommandation du format svg ;
* ratio : carré ;
* taille minimale recommandée (si non vectorielle): 128x128.

### Je ne veux pas partager les données de mes clients

Les métriques contenus pertinents à mettre dans les fichiers _properties_, ce ne sont pas les données de clients en particulier, mais les données agglomérées sur les services. Exemple : le nombre total de page d'une ferme de wikis et non pas le nombre de pages de chaque client de la ferme.
